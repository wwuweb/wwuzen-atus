<?php
/**
 * @file
 * Panel Layout plugin definition.
 */

$plugin = array(
  'title' => t('Centered One Column'),
  'category' => t('Western'),
  'theme' => 'centered-one-col',
  'css' => '../../css/layouts/centered-one-col.css',
  'regions' => array(
    'main' => t('Main'),
  ),
);
