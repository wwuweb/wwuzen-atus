<?php
/**
 * @file
 * Centered One Col panel layout.
 */
?>
<div class="panel-display centered-one-col centered-one-col-wrapper" <?php if (!empty($css_id)) {print "id=\"{$css_id}\"";} ?>>
  <div class="centered-one-col centered-one-col-main">
  <?php print $content['main']; ?>
  </div>
</div>
