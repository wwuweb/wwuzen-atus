<?php
/**
 * @file
 * Panel Layout plugin definition.
 */

$plugin = array(
  'title' => t('ATUS Home Page'),
  'category' => t('Western'),
  'theme' => 'atus-home-page',
  'css' => '../../css/layouts/atus-home-page.css',
  'regions' => array(
    'content' => t('Content'),
    'below_content' => t('Below Content'),
  ),
);
