<?php
/**
 * @file
 * ATUS Home Page layout.
 */
?>
<div class="panel-display atus-home-page atus-home-page-wrapper" <?php if (!empty($css_id)) {print "id=\"{$css_id}\"";} ?>>
  <div class="atus-home-page atus-home-page-content">
    <div class="center-content">
    <?php print $content['content']; ?>
    </div>
  </div>
  <div class="atus-home-page atus-home-page-below-content">
    <div class="center-content">
    <?php print $content['below_content']; ?>
    </div>
  </div>
</div>
